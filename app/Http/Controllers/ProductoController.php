<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ProductoResource;


class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $producto = Producto::with('Variantes')->get();
       
      
       return ProductoResource::collection($producto);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $validate = Validator::make($request->all(),[
            'nom_produc' => 'required|max:50',
            'pre_produc' => 'required|numeric|regex:/^[\d]{0,8}(\.[\d]{1,2})?$/',
            'des_produc' => 'required|max:50',
            'id_varproduc' => 'numeric|required',
             ]);


             if($validate->fails()){
     
                return response()->json([ 
       
                    'status' => 400,
                    'errors' => $validate->messages(),
    
                ]);
    
            }else{
     
                $producto = new Producto;
                $producto->nom_produc = $request->input('nom_produc');
                $producto->pre_produc = $request->input('pre_produc');
                $producto->des_produc = $request->input('des_produc');
                $producto->id_varproduc = $request->input('id_varproduc');
                $producto->save();
          

                return ProductoResource::make($producto);

           
          
          }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::with('Variantes')->find($id);
       
        
        if($producto){

        return ProductoResource::make($producto);
        
       
          }else{

            return response()->json([
    
            'status' => 404,
            'message' => 'lo siento, no existe el producto con ese id',
    
        ]);
    
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(),[
            'nom_produc' => 'required|max:50',
            'pre_produc' => 'required|numeric|regex:/^[\d]{0,8}(\.[\d]{1,2})?$/',
            'des_produc' => 'required|max:50',
            'id_varproduc' => 'numeric|required',
             ]);
     
             if($validate->fails()){
     
                return response()->json([ 
       
                    'status' => 400,
                    'errors' => $validate->messages(),
    
                ]);
    
            }else{

            $producto = Producto::find($id);
                
             if($producto){

                $producto->nom_produc = $request->input('nom_produc');
                $producto->pre_produc = $request->input('pre_produc');
                $producto->des_produc = $request->input('des_produc');
                $producto->id_varproduc = $request->input('id_varproduc');
                $producto->update();
          

                return ProductoResource::make($producto);

              }else{

                return response()->json([

                    'status' => 404,
                    'message' => 'No se encontro algun producto con tal id',
            
                ]);


              }

            }
             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::find($id);

        if($producto){

        $producto->delete();

        return ProductoResource::make($producto);

        }else{

            return response()->json([
    
                'status' => 404,
                'message' => 'lo siento, no existe el producto con ese id',
        
            ]);

        }

    }
}
