<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       return [
             'id' => $this->id,
             'nom_produc' => $this->nom_produc,
             'pre_produc' => $this->pre_produc,
             'des_produc' => $this->des_produc,
             'id_varproduc' => $this->id_varproduc,
             'variantes' => $this->variantes,
       ];
    }
}
