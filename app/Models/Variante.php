<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variante extends Model
{
    use HasFactory;

    protected $table = 'variante';

    public function Productos()
    {
        return $this->belongsToMany('App\Models\Producto');
     }

    protected $fillable = [
        'id',
        'var_produc',
    ];


    public $timestamps = false;
}
