<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    public function Variantes()
    {
        return $this->hasMany('App\Models\Variante', 'id','id_varproduc');
     }

     protected $table = 'producto';

    protected $fillable = [
        'id',
        'nom_produc',
        'pre_produc',
        'des_produc',
        'id_varproduc',
    ];


    public $timestamps = false;
}
