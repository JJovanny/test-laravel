<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('producto')->insert([
            'id' => 1,
            'nom_produc' => 'polo ralph lauren',
            'pre_produc' => 45.12,
            'des_produc' => 'camisa manga corta, color azul marino talla S',
            'id_varproduc' => 1,
        
        ]);
        

    }
}
