<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class VarianteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('variante')->insert([
            'id' => 1,
            'var_produc' => 'camiseta',
       ]);

       DB::table('variante')->insert([
        'id' => 2,
        'var_produc' => 'pantalon',
   ]);
    }
}
