## 1. **Cree una base de datos llamada testing en su gestor de base de datos relacional**

## 2. **Descomprima la carpeta test-laravel**

si utiliza xampp, ubiquela en 

- xampp/htdocs/

## 2. **Ingrese en la carpeta, abra la terminal por medio del path de la misma:**
C:\xampp\htdocs\test

escriba cmd y dele enter

## 4. **Una vez abierta la terminal escriba el siguiente comando:**

- php artisan migrate --seed

esto servira para realizar las migraciones a la base de datos con datos incluidos

## 5. **luego escriba**

- php artisan ser

Y pruebe la app por medio de las siguientes rutas

GET : le permitira ver un producto selecionado por medio de su id
http://localhost:8000/producto/{id}/edit

GET : le permitira ver todos los productos 
http://localhost:8000/producto/create

POST : le permitira crear productos
http://localhost:8000/producto

PUT : le permitira actualizar productos por medio de su id y los datos a cambiar
http://localhost:8000/producto/{id}

DELETE : le permitira eliminar el producto por medio de id
http://localhost:8000/producto/{id}






